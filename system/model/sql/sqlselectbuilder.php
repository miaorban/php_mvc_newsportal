<?php

namespace System\Model\Sql;

class SqlSelectBuilder extends SqlQueryBuilder
{
	private $limit = "";
	private $order = [];
	
	public function orderBy($field, $asc=true)
	{
		$this->order[] = $field." ".($asc ? "ASC" : "DESC");
	}
	public function limit($offset=0, $length=1)
	{
		$this->limit = " LIMIT ".$offset.",".$length;
	}

	protected function build()
	{
		$this->result = "SELECT * FROM `".$this->table."` ";
		if($this->where) $this->result .= (" ".$this->where);
		if($this->order) $this->result .= (" ORDER BY ".implode(", ", $this->order));
		$this->result .= $this->limit; 
		$this->result .= ";"; 
	}
}

