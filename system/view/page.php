<?php

namespace System\View;

class Page
{
	public static $title = "Website";
	public static $js = [];
	public static $css = [];
	
	public static function Begin()
	{
		if(self::$css)
		{
			foreach(self::$css as &$css)
			{
				$css = '<link rel="stylesheet" href="'.$css.'">';
			}
			self::$css = implode("", self::$css); 
		}
		else self::$css = "";
		
		if(self::$js)
		{
			foreach(self::$js as &$js)
			{
				$js = '<script src="'.$js.'"></script>';
			}
			self::$js = implode("", self::$js);
		}
		else self::$js = "";
		
		$html = '<!DOCTYPE html>
				<html>
				<head>
					<meta charset="UTF-8">
					<title>'.self::$title.'</title>
					'.self::$css.'
					'.self::$js.'
				</head>
				<body>';
		
		self::$title = null;
		self::$js = null;
		self::$css = null;
		
		return $html;
	}
	public static function End()
	{
		return '</body></html>';
	}
}

