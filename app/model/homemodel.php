<?php

namespace App\Model;

class HomeModel
{
	public static function GetCategories()
	{
		return
		[
			1 => "Belföld",
			2 => "Külföld",
			3 => "Cuki állatok"
		];
	}
	public static function GetArticles($count, $page)
	{
		$table = self::TableName();
		$db = \System\Model\Database::GetDefaultConnection();

		$builder = new \System\Model\Sql\SqlSelectBuilder($table);
		$builder->where("public != 0");
		$builder->limit( ($page-1)*$count , $count+1);
		$builder->orderBy("created", false);

		$sql = $builder->getResult();
		$db->execute($sql);

		if($articles = $db->getResult('App\Model\Article'))
		{
			$categories = self::GetCategories();

			foreach($articles as $article)
			{
				$article->category = $categories[$article->category];
			}

			return $articles;
		}
	}
	public static function GetArticlesByCategory($category)
	{
		$table = self::TableName();
		$db = \System\Model\Database::GetDefaultConnection();

		$builder = new \System\Model\Sql\SqlSelectBuilder($table);
		$builder->where("public != 0 AND category=:category");
		$builder->orderBy("created", false);

		$sql = $builder->getResult();
		$db->execute($sql, ['category' => $category]);

		if($articles = $db->getResult('App\Model\Article'))
		{
			$categories = self::GetCategories();

			foreach($articles as $article)
			{
				$article->category = $categories[$article->category];
			}

			return $articles;
		}
	}
	public static function GetArticleById($id)
	{
		$table = self::TableName();
		$db = \System\Model\Database::GetDefaultConnection();

		$builder = new \System\Model\Sql\SqlSelectBuilder($table);
		$builder->where("blogpost_id = :id");

		$sql = $builder->getResult();
		$db->execute($sql, ['id' => $id]);

		$result = $db->getResult('App\Model\Article');

		if($result) return $result[0];
		return null;
	}

	public static function TableName()
	{
		return \Config::$dbPrefix."__blogpost";
	}
}
