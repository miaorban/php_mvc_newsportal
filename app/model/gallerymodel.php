<?php
namespace App\Model;

class GalleryModel
{
	public static function GetCategories()
	{
		return
		[
			[ 'cid' => 1, 'url' => "c#", 'name' => "C# Programozás" ],
			[ 'cid' => 2, 'url' => "java", 'name' => "Java Programozás" ],
			[ 'cid' => 3, 'url' => "php", 'name' => "Php Programozás" ],
			[ 'cid' => 4, 'url' => "python", 'name' => "Python Programozás" ],
			[ 'cid' => 5, 'url' => "adatbazis", 'name' => "Adatbáziskezelés" ]
		];
	}
	public static function GetAlbumsByCategory($category)
	{
		$data =
		[
			'c#' =>
			[
				[ 'aid' => 1, 'url' => "c#alapok", 'name' => "C# Alapok" ],
				[ 'aid' => 2, 'url' => "c#peldak", 'name' => "C# Példák" ]
			],
			'java' =>
			[
				[ 'aid' => 3, 'url' => "javaalapok", 'name' => "Java Alapok" ],
				[ 'aid' => 4, 'url' => "javapeldak", 'name' => "Java Példák" ]
			],
			'php' =>
			[
				[ 'aid' => 5, 'url' => "phphalapok", 'name' => "Php Alapok" ],
				[ 'aid' => 6, 'url' => "phppeldak", 'name' => "Php Példák" ]
			],
			'python' => [
				[ 'aid' => 7, 'url' => "pythonalapok", 'name' => "Python Alapok" ],
				[ 'aid' => 8, 'url' => "pythonpeldak", 'name' => "Python Példák" ]
			],
			'adatbazis' => [
				[ 'aid' => 9, 'url' => "adatbazisalapok", 'name' => "Adatbáziskezelés Alapok" ],
				[ 'aid' => 10, 'url' => "adatbazispeldak", 'name' => "Adatbáziskazelés Példák" ]
			]
		];

		return $data[$category];
	}
}
