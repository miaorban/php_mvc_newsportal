<?php

namespace App\Model;

class Article
{
	public $blogpost_id;
	public $category;
	public $title;
	public $intro;
	public $image;
	public $content;
	public $created;
	public $public;
}

