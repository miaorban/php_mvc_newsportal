<?php

namespace App\Model;
use System\Utils\Request as R;

class PostModel
{
	public static function InsertPostedData()
	{
		return self::Insert( self::PrepareData() );
	}
	
	private static function PrepareData()
	{
		$data = 
		[
			'category' => R::Post("category"),
			'title' => R::Post("title"),
			'intro' => R::Post("intro"),
			'image' => self::UploadImage(),
			'content' => R::Post("content"),
			'created' => date("Y-m-d H:i:s"),
			'public' => 1
		];
		return $data;
	}
	private static function UploadImage()
	{
		if($_FILES['image'] && $_FILES['image']['tmp_name'])
		{
			$name = $_FILES['image']['name'];
			move_uploaded_file($_FILES['image']['tmp_name'], "media/".$name);
			return $name;
		}
		return "";
	}
	private static function Insert($data)
	{
		$table = \App\Model\HomeModel::TableName();
		$builder = new \System\Model\Sql\SqlManipulationBuilder($table);
		$builder->setFields(["category", "title", "intro", "image", "content", "created", "public"]);
		$sql = $builder->getResult();
		
		$db = \System\Model\Database::GetDefaultConnection();
		return $db->execute($sql, $data);
	}
}