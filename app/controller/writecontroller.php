<?php

namespace App\Controller;
use System\View\Form as F;

class WriteController
{
	public static function Entry()
	{
		\App\View\StandardView::PageBegin("IT Blog");

		self::PostFormSendRequest();
		self::PostForm();

		\App\View\StandardView::PageEnd();
	}

	private static function PostForm()
	{
		$form = new F\InputForm("writeForm", "POST", "", "Létrehoz");

		$form
		->addField(new F\InputSelect("Kategória", "category", \App\Model\HomeModel::GetCategories(), "category"))
		->addField(new F\InputField("Cím", "title", "text", "title"))
		->addField(new F\InputField("Bevezető szöveg", "intro", "text", "intro"))
		->addField(new F\InputText("Tartalom", "content", "content"));

		$html = $form->getHTML();
		\System\View\View::Out($html);
	}
	private static function PostFormSendRequest()
	{
		if(\System\Utils\Request::Post("writeForm"))
		{
			var_dump(
			\App\Model\PostModel::InsertPostedData()
			);
		}
	}
}
