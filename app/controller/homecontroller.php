<?php

namespace App\Controller;


class HomeController
{
	public static function Entry($listPage=true, $id=1)
	{
		\App\View\StandardView::PageBegin("NEWS. A leghitelesebb hírforrás.");

		if(!$listPage) self::ArticleReadingPage($id);
		else self::ArticleListPage($id);

		\App\View\StandardView::PageEnd();
	}
	public static function EntryCat($category)
	{
		\App\View\StandardView::PageBegin("Kategória szerint");
		\App\View\HomeView::ShowArticles(
			\App\Model\HomeModel::GetArticlesByCategory($category)
		);
		\App\View\StandardView::PageEnd();
	}

	private static function ArticleListPage($page)
	{
		$count = \Config::$postCountPerPage;
		$nextPage = false;
		$articles = \App\Model\HomeModel::GetArticles($count, $page);

		if(count($articles) > $count)
		{
			$nextPage = true;
			array_pop($articles);
		}

		\App\View\HomeView::ShowArticles($articles);
		\App\View\HomeView::ShowPagination($page, $nextPage);
	}
	private static function ArticleReadingPage($article)
	{
		\App\View\HomeView::ShowArticle(
			\App\Model\HomeModel::GetArticleById($article)
		);
	}
}
