<?php

namespace App\Controller;

class InstallController
{
	public static function Entry($key)
	{
		if($key == "12345")
		{
			$table = \Config::$dbPrefix."__blogpost";
			$builder = new \System\Model\Sql\CreateTableBuilder($table);
			$builder
				->addField("blogpost_id", "INT")->setAutoIncrement()
				->addField("category", "TINYINT", 1, "UNSIGNED")
				->addField("title", "VARCHAR", 100)
				->addField("intro", "VARCHAR", 300)
				->addField("image", "VARCHAR", 75)
				->addField("content", "TEXT")
				->addField("created", "DATETIME")
				->addField("public", "TINYINT", 1, "UNSIGNED")
				->addPrimaryKey("blogpost_id");
			
			$sql = $builder->getResult();
			
			$db = \System\Model\Database::GetDefaultConnection();
			$db->execute($sql);
		}
	}
}

