<?php if(!class_exists("System\Utils\AutoLoad")) die();

new \System\Model\Database(Config::$dbType, Config::$dbHost, 
	Config::$dbName, Config::$dbUser, Config::$dbPass);

\System\Control\Router::
Route(null, function()
{
	\App\Controller\HomeController::Entry();
})
->Route("posts", function($page=1)
{
	\App\Controller\HomeController::Entry(true, $page);
})
->Route("category", function($category=1)
{
	\App\Controller\HomeController::EntryCat($category);
})
->Route("read", function($article=null)
{
	\App\Controller\HomeController::Entry(false, $article);
})
->Route("write", function()
{
	\App\Controller\WriteController::Entry();
})
->Route("install", function($key=null)
{
	\App\Controller\InstallController::Entry($key);
})
->DefaultMethod(function($params)
{
	\App\Controller\NotFoundController::Entry();
});