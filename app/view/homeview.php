<?php

namespace App\View;

class HomeView
{
	public static function ShowArticles($model)
	{
		$GLOBALS['model'] = $model;
		include 'app/view/template/home-articles.php';
		unset($GLOBALS['model']);
	}
	public static function ShowPagination($page, $next)
	{
		\System\View\View::Out('<nav><ul>
				'.($page > 1 ? '<li><a href="?posts/'.($page-1).'">Újabb cikkek</a></li>' : '').'
				'.($next ? '<li><a href="?posts/'.($page+1).'">Korábbi cikkek</a></li>' : '').'
			</ul></nav>');
	}
	public static function ShowArticle($model)
	{
		$html = file_get_contents('app/view/template/home-article-one.php');
		$parser = new \System\View\ViewParser($html);
		\System\View\View::Out( $parser->render($model) );
	}
}

