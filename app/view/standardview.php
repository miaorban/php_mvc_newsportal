<?php

namespace App\View;
use System\View;

class StandardView
{
	public static function PageBegin($title)
	{
		View\Page::$title = $title;
		View\Page::$css = ["src/style.css"];
		View\Page::$js = ["src/script.js"];
		View\View::Out( View\Page::Begin() );
		
		$GLOBALS['model'] = \App\Model\HomeModel::GetCategories();
		include 'app/view/template/page-begin.php';
		unset($GLOBALS['model']);
	}
	public static function PageEnd()
	{
		require 'app/view/template/page-end.php';
		View\View::Out( View\Page::End() );
	}
}
