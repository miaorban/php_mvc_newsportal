<?php foreach($model as $m): ?>
	<div class="card text-white bg-primary mb-3" style="max-width: 60%;">
		<article class="blogPostPreview">
			<?php $link = "?read/".$m->blogpost_id; ?>
			<div class="card-header"> <?= $m->category ?></div>
 			<div class="card-body">
		 		<h3 class="card-title"><a href="<?= $link ?>"><?= $m->title ?></a></h3>
				<p class="card-text"> <?= $m->created ?> </p>
				<p></p>
				<h5 class="card-text"><?= $m->intro ?></h5>
				<h6 class="card-text"><a href="<?= $link ?>">Bővebben</a><h6>
			</div>
	</div>
<?php endforeach; ?>
