<div class="card text-white bg-primary mb-3" style="max-width: 60%;">
		<article class="home">
			<div class="card-header"> <h3> {{title|Title of Article}} </h3> </div>
 			<div class="card-body">
		 		<h4 class="card-title">{{intro|Intro of Article}}</h4>
				<p class="card-text"> {{created}} </p>
				<img src="media/{{image|http://mosaicon.hu/wallpapers/unnepi/2k-fullhd-hatterkep-wallpaper-mosaicon-tuzijatek-46026300_largethumb.jpg}}" alt="">
				<p></p>
				<h6 class="card-text">{{content|Content of Article}}<h6>
			</div>
		</article>
</div>
